﻿namespace BlueStacks.ControlPanel.UI
{
    partial class BstCplContent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlContentHolder = new System.Windows.Forms.Panel();
            this.pnlCplButtonHolder = new BlueStacks.ControlPanel.UI.Controls.BstBackgroundPanel();
            this.btnCplAbout = new BlueStacks.ControlPanel.UI.Controls.BstCplButton();
            this.btnCplMisc = new BlueStacks.ControlPanel.UI.Controls.BstCplButton();
            this.btnCplFolders = new BlueStacks.ControlPanel.UI.Controls.BstCplButton();
            this.btnCplDevice = new BlueStacks.ControlPanel.UI.Controls.BstCplButton();
            this.btnCplLauncher = new BlueStacks.ControlPanel.UI.Controls.BstCplButton();
            this.panel1 = new BlueStacks.ControlPanel.UI.Controls.BstBackgroundPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblOS = new System.Windows.Forms.Label();
            this.lblRAM = new System.Windows.Forms.Label();
            this.lblDataDir = new System.Windows.Forms.Label();
            this.lblInstallDir = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblName = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.pnlCplButtonHolder.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackgroundImage = global::BlueStacks.Properties.Resources.RibbonStatus;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 379);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(575, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.statusLabel.ForeColor = System.Drawing.Color.Gainsboro;
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(43, 17);
            this.statusLabel.Text = "Ready.";
            // 
            // pnlContentHolder
            // 
            this.pnlContentHolder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.pnlContentHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContentHolder.Location = new System.Drawing.Point(119, 100);
            this.pnlContentHolder.Name = "pnlContentHolder";
            this.pnlContentHolder.Size = new System.Drawing.Size(456, 279);
            this.pnlContentHolder.TabIndex = 4;
            // 
            // pnlCplButtonHolder
            // 
            this.pnlCplButtonHolder.BackColor = System.Drawing.Color.Transparent;
            this.pnlCplButtonHolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCplButtonHolder.Controls.Add(this.btnCplAbout);
            this.pnlCplButtonHolder.Controls.Add(this.btnCplMisc);
            this.pnlCplButtonHolder.Controls.Add(this.btnCplFolders);
            this.pnlCplButtonHolder.Controls.Add(this.btnCplDevice);
            this.pnlCplButtonHolder.Controls.Add(this.btnCplLauncher);
            this.pnlCplButtonHolder.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlCplButtonHolder.Location = new System.Drawing.Point(0, 100);
            this.pnlCplButtonHolder.Name = "pnlCplButtonHolder";
            this.pnlCplButtonHolder.PaintedImage = global::BlueStacks.Properties.Resources.RibbonV;
            this.pnlCplButtonHolder.Size = new System.Drawing.Size(119, 279);
            this.pnlCplButtonHolder.TabIndex = 3;
            // 
            // btnCplAbout
            // 
            this.btnCplAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplAbout.BackColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplAbout.BackColorSelected = System.Drawing.Color.DimGray;
            this.btnCplAbout.BorderColor = System.Drawing.Color.Gray;
            this.btnCplAbout.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnCplAbout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btnCplAbout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnCplAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCplAbout.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCplAbout.ForeColorNormal = System.Drawing.Color.WhiteSmoke;
            this.btnCplAbout.ForeColorSelected = System.Drawing.Color.White;
            this.btnCplAbout.Location = new System.Drawing.Point(12, 195);
            this.btnCplAbout.Name = "btnCplAbout";
            this.btnCplAbout.Selected = false;
            this.btnCplAbout.Size = new System.Drawing.Size(107, 36);
            this.btnCplAbout.TabIndex = 4;
            this.btnCplAbout.Tag = "Author and software information, copyright notice, and end-user agreement.";
            this.btnCplAbout.TagId = 4;
            this.btnCplAbout.Text = "About";
            this.btnCplAbout.UseVisualStyleBackColor = false;
            this.btnCplAbout.Click += new System.EventHandler(this.NavCplButton_Clicked);
            // 
            // btnCplMisc
            // 
            this.btnCplMisc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplMisc.BackColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplMisc.BackColorSelected = System.Drawing.Color.DimGray;
            this.btnCplMisc.BorderColor = System.Drawing.Color.Gray;
            this.btnCplMisc.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnCplMisc.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btnCplMisc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnCplMisc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCplMisc.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCplMisc.ForeColorNormal = System.Drawing.Color.WhiteSmoke;
            this.btnCplMisc.ForeColorSelected = System.Drawing.Color.White;
            this.btnCplMisc.Location = new System.Drawing.Point(12, 101);
            this.btnCplMisc.Name = "btnCplMisc";
            this.btnCplMisc.Selected = false;
            this.btnCplMisc.Size = new System.Drawing.Size(107, 36);
            this.btnCplMisc.TabIndex = 3;
            this.btnCplMisc.Tag = "Contains options to tweak BlueStacks\' services, startup entries, and log files.";
            this.btnCplMisc.TagId = 3;
            this.btnCplMisc.Text = "Miscellaneous";
            this.btnCplMisc.UseVisualStyleBackColor = false;
            this.btnCplMisc.Click += new System.EventHandler(this.NavCplButton_Clicked);
            // 
            // btnCplFolders
            // 
            this.btnCplFolders.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplFolders.BackColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplFolders.BackColorSelected = System.Drawing.Color.DimGray;
            this.btnCplFolders.BorderColor = System.Drawing.Color.Gray;
            this.btnCplFolders.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnCplFolders.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btnCplFolders.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnCplFolders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCplFolders.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCplFolders.ForeColorNormal = System.Drawing.Color.WhiteSmoke;
            this.btnCplFolders.ForeColorSelected = System.Drawing.Color.White;
            this.btnCplFolders.Location = new System.Drawing.Point(12, 143);
            this.btnCplFolders.Name = "btnCplFolders";
            this.btnCplFolders.Selected = false;
            this.btnCplFolders.Size = new System.Drawing.Size(107, 36);
            this.btnCplFolders.TabIndex = 2;
            this.btnCplFolders.Tag = "Add, edit, or remove folders in the filesystem that will be mounted on the Androi" +
                "d device\'s SD card.";
            this.btnCplFolders.TagId = 2;
            this.btnCplFolders.Text = "Shared Folders";
            this.btnCplFolders.UseVisualStyleBackColor = false;
            this.btnCplFolders.Visible = false;
            this.btnCplFolders.Click += new System.EventHandler(this.NavCplButton_Clicked);
            // 
            // btnCplDevice
            // 
            this.btnCplDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplDevice.BackColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplDevice.BackColorSelected = System.Drawing.Color.DimGray;
            this.btnCplDevice.BorderColor = System.Drawing.Color.Gray;
            this.btnCplDevice.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnCplDevice.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btnCplDevice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnCplDevice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCplDevice.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCplDevice.ForeColorNormal = System.Drawing.Color.WhiteSmoke;
            this.btnCplDevice.ForeColorSelected = System.Drawing.Color.White;
            this.btnCplDevice.Location = new System.Drawing.Point(12, 59);
            this.btnCplDevice.Name = "btnCplDevice";
            this.btnCplDevice.Selected = false;
            this.btnCplDevice.Size = new System.Drawing.Size(107, 36);
            this.btnCplDevice.TabIndex = 1;
            this.btnCplDevice.Tag = "Contains settings for adjusting screen resolution and allocated memory.";
            this.btnCplDevice.TagId = 1;
            this.btnCplDevice.Text = "Device Settings";
            this.btnCplDevice.UseVisualStyleBackColor = false;
            this.btnCplDevice.Click += new System.EventHandler(this.NavCplButton_Clicked);
            // 
            // btnCplLauncher
            // 
            this.btnCplLauncher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplLauncher.BackColorNormal = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCplLauncher.BackColorSelected = System.Drawing.Color.DimGray;
            this.btnCplLauncher.BorderColor = System.Drawing.Color.Gray;
            this.btnCplLauncher.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btnCplLauncher.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btnCplLauncher.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.btnCplLauncher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCplLauncher.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCplLauncher.ForeColorNormal = System.Drawing.Color.WhiteSmoke;
            this.btnCplLauncher.ForeColorSelected = System.Drawing.Color.White;
            this.btnCplLauncher.Location = new System.Drawing.Point(12, 17);
            this.btnCplLauncher.Name = "btnCplLauncher";
            this.btnCplLauncher.Selected = false;
            this.btnCplLauncher.Size = new System.Drawing.Size(107, 36);
            this.btnCplLauncher.TabIndex = 0;
            this.btnCplLauncher.Tag = "Contains options for the launcher and the Control Panel.";
            this.btnCplLauncher.TagId = 0;
            this.btnCplLauncher.Text = "Launcher";
            this.btnCplLauncher.UseVisualStyleBackColor = false;
            this.btnCplLauncher.Click += new System.EventHandler(this.NavCplButton_Clicked);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblOS);
            this.panel1.Controls.Add(this.lblRAM);
            this.panel1.Controls.Add(this.lblDataDir);
            this.panel1.Controls.Add(this.lblInstallDir);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblVersion);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.PaintedImage = global::BlueStacks.Properties.Resources.RibbonH;
            this.panel1.Size = new System.Drawing.Size(575, 100);
            this.panel1.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Gainsboro;
            this.label6.Location = new System.Drawing.Point(118, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "BlueStacks Data Directory:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gainsboro;
            this.label5.Location = new System.Drawing.Point(118, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "BlueStacks Install Directory:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gainsboro;
            this.label4.Location = new System.Drawing.Point(118, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "Available RAM:";
            // 
            // lblOS
            // 
            this.lblOS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOS.AutoEllipsis = true;
            this.lblOS.BackColor = System.Drawing.Color.Transparent;
            this.lblOS.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOS.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblOS.Location = new System.Drawing.Point(247, 39);
            this.lblOS.Name = "lblOS";
            this.lblOS.Size = new System.Drawing.Size(323, 12);
            this.lblOS.TabIndex = 3;
            this.lblOS.Text = "%OS%";
            // 
            // lblRAM
            // 
            this.lblRAM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRAM.AutoEllipsis = true;
            this.lblRAM.BackColor = System.Drawing.Color.Transparent;
            this.lblRAM.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRAM.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblRAM.Location = new System.Drawing.Point(247, 53);
            this.lblRAM.Name = "lblRAM";
            this.lblRAM.Size = new System.Drawing.Size(323, 12);
            this.lblRAM.TabIndex = 3;
            this.lblRAM.Text = "%RAM%";
            // 
            // lblDataDir
            // 
            this.lblDataDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDataDir.AutoEllipsis = true;
            this.lblDataDir.BackColor = System.Drawing.Color.Transparent;
            this.lblDataDir.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataDir.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblDataDir.Location = new System.Drawing.Point(247, 82);
            this.lblDataDir.Name = "lblDataDir";
            this.lblDataDir.Size = new System.Drawing.Size(323, 12);
            this.lblDataDir.TabIndex = 3;
            this.lblDataDir.Text = "%DataDir%";
            // 
            // lblInstallDir
            // 
            this.lblInstallDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInstallDir.AutoEllipsis = true;
            this.lblInstallDir.BackColor = System.Drawing.Color.Transparent;
            this.lblInstallDir.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInstallDir.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblInstallDir.Location = new System.Drawing.Point(247, 67);
            this.lblInstallDir.Name = "lblInstallDir";
            this.lblInstallDir.Size = new System.Drawing.Size(323, 12);
            this.lblInstallDir.TabIndex = 3;
            this.lblInstallDir.Text = "%InstallDir%";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gainsboro;
            this.label3.Location = new System.Drawing.Point(118, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "Operating System:";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.LightGray;
            this.lblVersion.Location = new System.Drawing.Point(446, 16);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(106, 19);
            this.lblVersion.TabIndex = 2;
            this.lblVersion.Text = "version 0.0.0.0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::BlueStacks.Properties.Resources.CplLogo;
            this.pictureBox1.Location = new System.Drawing.Point(5, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 86);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 21F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblName.Location = new System.Drawing.Point(112, 1);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(342, 38);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "BlueStacks Control Panel";
            // 
            // BstCplContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlContentHolder);
            this.Controls.Add(this.pnlCplButtonHolder);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "BstCplContent";
            this.Size = new System.Drawing.Size(575, 401);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.pnlCplButtonHolder.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private BlueStacks.ControlPanel.UI.Controls.BstBackgroundPanel pnlCplButtonHolder;
        private BlueStacks.ControlPanel.UI.Controls.BstBackgroundPanel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblOS;
        private System.Windows.Forms.Label lblRAM;
        private System.Windows.Forms.Label lblDataDir;
        private System.Windows.Forms.Label lblInstallDir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private Controls.BstCplButton btnCplLauncher;
        private Controls.BstCplButton btnCplAbout;
        private Controls.BstCplButton btnCplMisc;
        private Controls.BstCplButton btnCplFolders;
        private Controls.BstCplButton btnCplDevice;
        private System.Windows.Forms.Panel pnlContentHolder;

    }
}
