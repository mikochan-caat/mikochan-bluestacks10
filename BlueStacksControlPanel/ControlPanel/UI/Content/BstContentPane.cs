using System;
using System.Drawing;
using System.Windows.Forms;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI.Content
{
    internal partial class BstContentPane : UserControl
    {
        private static readonly Image BG = global::BlueStacks.Properties.Resources.Content;

        protected BstCplContent ControlPanel
        {
            get;
            private set;
        }

        public bool HasControlPanel
        {
            get
            {
                return this.ControlPanel != null;
            }
        }

        public BstContentPane()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);
            this.InitializeComponent();
            this.Visible = false;
        }

        public virtual void OnApplicationClosing()
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!Designer.InDesignMode(this)) {
                Control ctr = this;
                do {
                    ctr = ctr.Parent;
                } while (!(ctr is BstCplContent) && ctr.Parent != null);
                if (ctr != null) {
                    this.ControlPanel = ctr as BstCplContent;
                }
                this.Dock = DockStyle.Fill;
            }
        }

        protected sealed override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
            e.Graphics.DrawImageUnscaled(BG, 0, 0);
        }
    }
}
