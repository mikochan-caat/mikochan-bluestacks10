using System;
using System.Windows.Forms;
using BlueStacks.Management.Config;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI.Content
{
    internal sealed partial class BstDevicePane : BstContentPane
    {
        private PropertyDiff diff;

        private int maxMem;
        private bool saveMem;

        public BstDevicePane() : base()
        {
            this.InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.diff = new PropertyDiff();
            this.ControlPanel.RegisterStatusDescription(this.txtWindowWidth);
            this.ControlPanel.RegisterStatusDescription(this.txtWindowHeight);
            this.ControlPanel.RegisterStatusDescription(this.txtTabletWidth);
            this.ControlPanel.RegisterStatusDescription(this.txtTabletHeight);
            this.ControlPanel.RegisterStatusDescription(this.txtAllocatedRAM);
            this.ControlPanel.RegisterStatusDescription(this.chkFullscreen);
            this.ControlPanel.RegisterStatusDescription(this.prgMemAvailable);
            this.ControlPanel.RegisterStatusDescription(this.prgMemAllocated);
            this.ControlPanel.RegisterStatusDescription(this.btnSave);
            this.ControlPanel.RegisterSaveButton(this.btnSave, this, this.Save_Validate, this.Save_Start, this.Save_End);
            this.txtWindowHeight.MaxValue = this.txtWindowWidth.MaxValue = Int32.MaxValue;
            this.txtTabletHeight.MaxValue = this.txtTabletWidth.MaxValue = Int32.MaxValue;
            this.txtAllocatedRAM.MaxValue = Int32.MaxValue;
            var props = this.ControlPanel.BstProperties;
            this.txtWindowWidth.Value = MathEx.Clamp(props.WindowWidth, this.txtWindowWidth.MinValue, this.txtWindowWidth.MaxValue);
            this.txtWindowHeight.Value = MathEx.Clamp(props.WindowHeight, this.txtWindowHeight.MinValue, this.txtWindowHeight.MaxValue);
            this.txtTabletWidth.Value = MathEx.Clamp(props.TabletWidth, this.txtTabletWidth.MinValue, this.txtTabletWidth.MaxValue);
            this.txtTabletHeight.Value = MathEx.Clamp(props.TabletHeight, this.txtTabletHeight.MinValue, this.txtTabletHeight.MaxValue);
            this.chkFullscreen.Checked = props.FullScreen;
            this.InitMemoryControls();
            this.SetPropertyChanges(true);
            this.CheckWindowTabletWarnings(false);
            if (!props.HasVideoSettings) {
                this.txtWindowWidth.Enabled = false;
                this.txtWindowHeight.Enabled = false;
                this.txtTabletWidth.Enabled = false;
                this.txtTabletHeight.Enabled = false;
                this.chkFullscreen.Enabled = false;
            }
        }

        public override void OnApplicationClosing()
        {
            this.txtTabletHeight.ValidateInput(false);
            this.txtTabletWidth.ValidateInput(false);
            this.txtWindowHeight.ValidateInput(false);
            this.txtWindowWidth.ValidateInput(false);
            this.txtAllocatedRAM.ValidateInput(false);
            this.SetPropertyChanges(false);
            if (this.diff.Changed) {
                if (Dialogs.WarnYesNo(this, "You have modified some BlueStacks Android Device " +
                                      "settings but have not saved yet.\n\n" +
                                      "Do you want to save these changes?",
                                      "Device Settings Pending Changes", Dialogs.Choice.Choice2)) {
                    this.Save_Start();
                    this.Save_End();
                }
            }
        }

        private void InitMemoryControls()
        {
            this.prgMemAvailable.Maximum = AppConstants.MaxAllocatedRAM;
            this.maxMem = Math.Min(AppConstants.MaxAllocatedRAM, Computer.TotalRAM);
            this.prgMemAllocated.Maximum = this.prgMemAvailable.Value = this.maxMem;
            this.lblMemAvailable.Text = String.Format("{0:000}", this.maxMem);
            if (Computer.TotalRAM >= AppConstants.MinRequiredRAM) {
                this.saveMem = true;
                this.txtAllocatedRAM.MinValue = AppConstants.MinAllocatedRAM;
                var props = this.ControlPanel.BstProperties;
                int alloc = props.AllocatedRAM;
                this.txtAllocatedRAM.Value = MathEx.Clamp(alloc, this.txtAllocatedRAM.MinValue, this.txtAllocatedRAM.MaxValue);
                if (alloc != this.txtAllocatedRAM.Value && this.txtAllocatedRAM.Value == AppConstants.MinAllocatedRAM) {
                    Dialogs.InfoOK(this, "The value for " + this.txtAllocatedRAM.PropertyName +
                                   " is too low.\nThe value will be set to the " +
                                   "minimum value of " + AppConstants.MinAllocatedRAM + ".",
                                   this.txtAllocatedRAM.PropertyName + " Too Low");
                    props.ClearLastError();
                    props.AllocatedRAM = this.txtAllocatedRAM.Value;
                    if (props.HasErrorMessage) {
                        Dialogs.WarnOK(this, "Could not set the " + this.txtAllocatedRAM.PropertyName +
                                       " to its minimum value.\n\nYou can try again by saving the settings.",
                                       "Value Set Failed");
                    }
                }
                this.SetMemoryAllocatedProgressBar();
            } else {
                this.saveMem = false;
                this.lblNotEnoughMem.Text += " Minimum system requirement is: " + AppConstants.MinRequiredRAM + " MB.";
                this.lblAllocRAM.Visible = false;
                this.txtAllocatedRAM.Visible = false;
                this.lblAllocMB.Visible = false;
                this.prgMemAllocated.Visible = false;
                this.lblNotEnoughMem.Visible = true;
            }
            this.txtAllocatedRAM.Tag = (this.txtAllocatedRAM.Tag as string) + AppConstants.MaxAllocatedRAM + " MB.";
        }

        private void SetPropertyChanges(bool commit)
        {
            if (this.ControlPanel.BstProperties.HasVideoSettings) {
                this.diff.SetProperty(this.txtWindowWidth.PropertyName, this.txtWindowWidth.Value);
                this.diff.SetProperty(this.txtWindowHeight.PropertyName, this.txtWindowHeight.Value);
                this.diff.SetProperty(this.txtTabletWidth.PropertyName, this.txtTabletWidth.Value);
                this.diff.SetProperty(this.txtTabletHeight.PropertyName, this.txtTabletHeight.Value);
                this.diff.SetProperty(this.chkFullscreen.Name, this.chkFullscreen.Checked);
            }
            if (this.saveMem) {
                this.diff.SetProperty(this.txtAllocatedRAM.PropertyName, this.txtAllocatedRAM.Value);
            }
            if (commit) {
                this.diff.Commit();
            }
        }

        private void CheckWindowTabletWarnings(bool showDialog)
        {
            var scr = Screen.FromControl(this.ParentForm).Bounds;
            this.lblWarnWindow.Visible = this.txtWindowWidth.Value > scr.Width || this.txtWindowHeight.Value > scr.Height;
            this.lblWarnTablet.Visible = this.txtTabletWidth.Value > 1920 || this.txtTabletHeight.Value > 1080;
            this.lblWarnRAM.Visible = this.txtAllocatedRAM.Value > this.maxMem;
            if (showDialog && (this.lblWarnRAM.Visible || this.lblWarnTablet.Visible || this.lblWarnWindow.Visible)) {
                string msg = "Your settings have triggered some warnings:\n\n" +
                             ((this.lblWarnRAM.Visible) ? this.lblWarnRAM.Text + "\n" : "") +
                             ((this.lblWarnWindow.Visible) ? this.lblWarnWindow.Text + "\n" : "") +
                             ((this.lblWarnTablet.Visible) ? this.lblWarnTablet.Text + "\n" : "") +
                             "\nNote: Some of these options when misconfigured MAY cause stability " +
                             "issues.\nYou have been warned.";
                Dialogs.WarnOK(this, msg, "Device Setting Warnings");
            } 
        }

        private void SetMemoryAllocatedProgressBar()
        {
            this.prgMemAllocated.Value = MathEx.Clamp(this.txtAllocatedRAM.Value, this.prgMemAllocated.Minimum, this.prgMemAllocated.Maximum);
        }

        private bool Save_Validate()
        {
            bool result = this.txtWindowWidth.ValidateInput(true) && this.txtWindowHeight.ValidateInput(true) &&
                          this.txtTabletWidth.ValidateInput(true) && this.txtTabletHeight.ValidateInput(true) &&
                          ((this.saveMem) ? this.txtAllocatedRAM.ValidateInput(true) : true);
            return result;
        }

        private void Save_Start()
        {
            var props = this.ControlPanel.BstProperties;
            props.ClearLastError();
            if (props.HasVideoSettings) {
                props.WindowWidth = this.txtWindowWidth.Value;
                props.WindowHeight = this.txtWindowHeight.Value;
                props.TabletWidth = this.txtTabletWidth.Value;
                props.TabletHeight = this.txtTabletHeight.Value;
                props.FullScreen = this.chkFullscreen.Checked;
            }
            if (this.saveMem) {
                props.AllocatedRAM = this.txtAllocatedRAM.Value;
            }
        }

        private void Save_End()
        {
            this.SetPropertyChanges(true);
            var props = this.ControlPanel.BstProperties;
            if (props.HasErrorMessage) {
                Dialogs.WarnOK(this, props.LastErrorMessage, "Property Save Error");
            }
            this.SetMemoryAllocatedProgressBar();
            this.CheckWindowTabletWarnings(true);
        }
    }
}
