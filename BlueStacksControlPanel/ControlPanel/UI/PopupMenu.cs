﻿using System.ComponentModel;
using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UI
{
    internal class PopupMenu : ContextMenu
    {
        [Browsable(true)]
        public new MenuItemCollection MenuItems
        {
            get
            {
                return base.MenuItems;
            }
        }
    }
}
