﻿using System;
using System.Windows.Forms;
using BlueStacks.Management;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI
{
    internal partial class MainForm
    {
        private BstProperties bstProps;

        public void FormLogicConstructor()
        {
            this.lblOS.Text = Computer.OSName;
            this.lblRAM.Text = Computer.TotalRAM + " MB";
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.bstProps = new BstProperties();
            if (this.bstProps.HasInstallDetails) {
                this.lblInstallDir.Text = this.bstProps.InstallPath;
                this.lblDataDir.Text = this.bstProps.DataPath;
            } else {
                Dialogs.ErrorOK("The application could not launch because the BlueStacks " +
                                "installation directory was not found.\n\nPress OK " +
                                "to exit the application.", "Initialization Error");
                this.Close();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.bstProps.Dispose();
        }
    }
}
