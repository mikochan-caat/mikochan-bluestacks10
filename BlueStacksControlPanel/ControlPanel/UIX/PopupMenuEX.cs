﻿using System.ComponentModel;
using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UI
{
    internal class PopupMenuEx : ContextMenu
    {
        [Browsable(true)]
        public new MenuItemCollection MenuItems
        {
            get
            {
                return base.MenuItems;
            }
        }
    }
}
