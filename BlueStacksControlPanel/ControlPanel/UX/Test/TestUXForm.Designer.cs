﻿namespace BlueStacks.ControlPanel.UX.Test
{
    partial class TestUXForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.appFormSkin1 = new BlueStacks.ControlPanel.UI.AppFormSkin();
            this.SuspendLayout();
            // 
            // appFormSkin1
            // 
            this.appFormSkin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.appFormSkin1.FixedSingle = false;
            this.appFormSkin1.Location = new System.Drawing.Point(0, 0);
            this.appFormSkin1.Name = "appFormSkin1";
            this.appFormSkin1.Size = new System.Drawing.Size(284, 261);
            this.appFormSkin1.Stretch = false;
            this.appFormSkin1.TabIndex = 0;
            // 
            // TestUXForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.appFormSkin1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(163, 38);
            this.Name = "TestUXForm";
            this.Text = "TestUXForm";
            this.Resize += new System.EventHandler(this.TestUXForm_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private BlueStacks.ControlPanel.UI.AppFormSkin appFormSkin1;



    }
}