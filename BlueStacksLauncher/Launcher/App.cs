using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;
using BlueStacks.Management;
using BlueStacks.Management.Config;
using BlueStacks.Management.Interop;
using BlueStacks.Management.Utilities;

namespace BlueStacks.Launcher
{
    internal class App
    {
        private bool logOutput;

        private int pollTime;

        public App(string[] args)
        {
            this.logOutput = args != null && args.Length > 0 &&
                             args.FirstOrDefault((arg) => {
                                return arg == "/logpipe";
                             }) != null;
        }

        public void Run()
        {
            var asm = Assembly.GetExecutingAssembly().GetName();
            string head1 = "BlueStacks Launcher (" + asm.Name + ") v" + asm.Version;
            string head2 = "Processor architecture: " + asm.ProcessorArchitecture;
            string head3 = "Launcher PID: " + Process.GetCurrentProcess().Id;
            this.Info(head1);
            this.Info(head2);
            this.Info(head3);
            this.Info("Searching for BlueStacks installation directory...");
            using (var props = new BstProperties()) {
                if (props.HasInstallDetails) {
                    this.Info("Found at: " + props.InstallPath);
                    this.Info("Loading Configuration...");
                    var config = new AppConfig(AppConstants.AppConfigPath);
                    string procName;
                    string procPath;
                    if (!config.GetOption<bool>(AppConstants.ExitBlueStacksOnWindowClose)) {
                        procName = BstProcess.HDServiceProcess;
                        procPath = props.HDServicePath;
                    } else {
                        procName = BstProcess.HDFrontendProcess;
                        procPath = props.HDFrontendPath;
                    }
                    if (!config.LoadSuccess) {
                        this.Warn("Configuration file not found. Loading defaults...");
                    }
                    this.pollTime = MathEx.Clamp(config.GetOption<int>(AppConstants.MaxProcessAttachTime, AppConstants.DefPollTime), AppConstants.MinPollTime, AppConstants.MaxPollTime);
                    this.Info("Checking for instance of " + procName + "...");
                    Process proc = null;
                    try {
                        proc = BstProcess.Get(BstProcess.HDFrontendProcess, props.HDFrontendPath);
                        bool watch = true;
                        if (proc == null) {
                            this.Info("An instance of " + procName + " was not found. Starting BlueStacks...");
                            BstProcess.RunAndWait(props.HDLauncherPath);
                            proc = BstProcess.Poll(procName, procPath, this.pollTime, this.PollProcessTimeout);
                            watch = proc != null;
                        } else {
                            this.Info("Found " + procName + " with PID of " + proc.Id + ". Waiting for input...");
                            if (!Dialogs.QuestionYesNo("An instance of BlueStacks is running.\nDo you " +
                                                       "want to attach this application to that process?",
                                                       "Process Already Running")) {
                                if (procName != BstProcess.HDFrontendProcess) {
                                    proc.Dispose();
                                    proc = BstProcess.Get(procName, procPath);
                                }
                                watch = false;
                            }
                        }
                        if (watch) {
                            this.Info("Successfully attached to BlueStacks. Getting process info...");
                            bool doQuit = true;
                            do {
                                this.SetBlueStacksWindowName(asm, props, true);
                                this.Info("Process PID is " + proc.Id + ". Standing by...");
                                proc.WaitForExit();
                                this.Info("Standby mode interrupted. Taking course of action...");
                                bool restart = true;
                                BstProcess.GetAndUse(BstProcess.HDRestartProcess, props.HDRestartPath,
                                                     (p) => {
                                                         if (p != null) {
                                                             this.Info("Detected " + BstProcess.HDRestartProcess + ". Waiting for BlueStacks to restart...");
                                                             p.WaitForExit();
                                                             if (BstProcess.Poll(BstProcess.HDServiceProcess, props.HDServicePath, this.pollTime, null) == null) {
                                                                 restart = false;
                                                                 this.Warn(BstProcess.HDServiceProcess + " suddenly exited. Waking from standby...");
                                                             }
                                                         } else {
                                                             this.Info("No restart process found. Waking from standby...");
                                                             restart = false;
                                                         }
                                                     });
                                if (restart) {
                                    this.Info("BlueStacks has restarted. Attempting to reattach to that process...");
                                    proc.Dispose();
                                    proc = BstProcess.Poll(procName, procPath, this.pollTime, this.PollProcessTimeout);
                                    if (proc == null) {
                                        this.Info("User did not attempt to reattach the launcher. Shutting down...");
                                        doQuit = false;
                                    } else {
                                        this.Info("Process reattach successful. Standing by...");
                                        continue;
                                    }
                                }
                                break;
                            } while (true);
                            if (doQuit) {
                                this.PerformShutdown(config, props, procName);
                            } else {
                                this.Info("Woken from standby. BlueStacks will not be terminated. The launcher will now shutdown...");
                            }
                        } else {
                            this.Info("User did not attach the launcher. Shutting down...");
                        }
                    } catch (Exception e) {
                        var dateTime = DateTime.Now;
                        string logName = "fatal-" + dateTime.Year + "-" + dateTime.Month + "-" + dateTime.Day + "-" + dateTime.ToFileTimeUtc() + ".log";
                        if (this.logOutput) {
                            this.Error("A fatal error has occured. See " + logName + " for more information.");
                        } else {
                            Dialogs.ErrorOK("A fatal error has occured. See " + logName + " for more information.\n\nError: " +
                                            e.Message + "\n\nPlease contact the author about any problems " +
                                            "that may occur.\n\nPress OK to terminate the application.",
                                            "Application Error");
                        }
                        File.WriteAllText(logName, "BlueStacks Launcher Crash Log\r\n" +
                                          "Crash Occured: " + dateTime.ToString() + "\r\n\r\n" +
                                          "Details:\r\n" + head1 + "\r\n" + head2 + "\r\n" + head3 +
                                          "\r\n\r\nError+Stack Trace:\r\n" + e + "\r\n\r\nPlease " +
                                          "mail me this log at mikochan.caat@gmail.com for debugging.\r\n");
                        if (Dialogs.QuestionYesNo("An unexpected error caused the BlueStacks Launcher to exit while " +
                                                  "BlueStacks is running.\n\nWould you like to shutdown BlueStacks?" +
                                                  "\n\nNote: You can answer 'No' and relaunch this application to " +
                                                  "cleanly shutdown BlueStacks.",
                                                  "BlueStacks Launcher Error Cleanup")) {
                            this.logOutput = false;
                            this.PerformShutdown(config, props, procName);
                        }
                        this.SetBlueStacksWindowName(asm, props, false);
                        App.Exit(BstLauncherCodes.FatalError);
                    } finally {
                        if (proc != null) {
                            proc.Dispose();
                        }
                    }
                } else {
                    if (this.logOutput) {
                        this.Error("BlueStacks install directory not found.");
                    } else {
                        Dialogs.ErrorOK("The program cannot launch because the BlueStacks " +
                                        "installation directory could not be found.\n\n" +
                                        "Press OK to terminate the application.", "Initialization Error");
                    }
                    App.Exit(BstLauncherCodes.NoInstallDirectory);
                }
            }
            this.Info("The launcher has successfully exited.");
        }

        public void NextInstance()
        {
            if (this.logOutput) {
                this.Error("An instance of the launcher is already running.");
            } else {
                Dialogs.WarnOK("An instance of this launcher is already running.\n\n" +
                               "Press OK to terminate the application.", "Application Already Running");
            }
            App.Exit(BstLauncherCodes.LauncherAlreadyRunning);
        }

        private void PerformShutdown(AppConfig config, BstProperties props, string procName)
        {
            if (procName == BstProcess.HDFrontendProcess) {
                this.Info("Woken from standby. BlueStacks main window has been closed.");
            } else {
                this.Info("Woken from standby. BlueStacks Agent has exited.");
            }
            this.Info("Quitting other BlueStacks processes...");
            BstProcess.RunAndWait(props.HDQuitPath);
            this.Info("Shutdown successful. Attempting to disable re-enabled services.");
            if (config.GetOption<bool>(AppConstants.DisableServicesOnExit, true)) {
                this.Info("Disabling re-enabled services...");
                using (var androidSvc = new BstService(BstService.Android))
                using (var updaterSvc = new BstService(BstService.Updater)) {
                    this.StopService(androidSvc);
                    this.StopService(updaterSvc);
                }
            } else {
                this.Info("Attempt aborted. Control panel option is set to false.");
            }
            this.Info("Attempting to clear log files at " + props.DataLogsPath);
            if (config.GetOption<bool>(AppConstants.LogAutoClear)) {
                this.Info("Checking log folder total file size...");
                int thresh = config.GetOption<int>(AppConstants.LogAutoClearThreshold);
                string[] files = props.GetDataLogFilePaths();
                bool clear;
                if (thresh == 0) {
                    this.Info("Check skipped. Threshold was set to 0 (always clear).");
                    clear = true;
                } else {
                    long size = files.Select((file) => {
                        try {
                            return new FileInfo(file).Length;
                        } catch {
                            return 0;
                        }
                    }).Sum() / (1024 * 1024);
                    this.Info("Total size: " + size + " MB, Auto-clear threshold: " + thresh + " MB.");
                    clear = size >= thresh;
                }
                if (clear) {
                    this.Info("Clearing log files...");
                    string err = Computer.DeleteFiles(files);
                    if (err != null) {
                        this.Warn("Not all log files were cleared. Reasons:\r\n" + err);
                    } else {
                        this.Info("All log files were successfully cleared.");
                    }
                } else {
                    this.Info("Total size is less than the threshold. Not clearing...");
                }
            } else {
                this.Info("Attempt aborted. Control panel option is set to false.");
            }
            this.Info("All tasks completed. Completing launcher shutdown...");
        }

        private bool PollProcessTimeout()
        {
            this.Warn("Failed to attach to BlueStacks within " + this.pollTime + " ms. Retrying...");
            return Dialogs.ErrorRetryCancel("The application failed to attach to the BlueStacks " +
                                            "process in a timely fashion.\n\nWould you like to try to " +
                                            "reattach again?\nIf not, the application will quit but will " +
                                            "not exit BlueStacks. You can then run this application again " +
                                            "to reattach to BlueStacks.", "Process Attach Failed");
        }

        private void StopService(BstService svc)
        {
            string name = svc.ServiceName;
            try {
                this.Info("Disabling " + name + "...");
                if (svc.StartMode != BstService.StartType.Unknown) {
                    if (svc.Status != ServiceControllerStatus.Stopped) {
                        this.Info(name + " was running. Stopping it first...");
                        svc.Stop();
                    }
                    svc.StartMode = BstService.StartType.Disabled;
                    this.Info("Successfully disabled " + name + ".");
                } else {
                    this.Warn("Could not find/identify " + name + ".");
                }
            } catch (Exception e) {
                this.Error("Could not disable " + name + ". Reason: " + e.Message);
            }
        }

        private void SetBlueStacksWindowName(AssemblyName asm, BstProperties props, bool attached)
        {
            string title;
            if (attached) {
                title = "BlueStacks App Player (BlueStacks Launcher v" + asm.Version + ")";
            } else {
                title = "BlueStacks App Player";
            }
            Process p = BstProcess.Poll(BstProcess.HDFrontendProcess, props.HDFrontendPath, this.pollTime, null);
            if (p != null) {
                bool fail = false;
                try {
                    bool stop = false;
                    if (p.MainWindowHandle != IntPtr.Zero) {
                        this.SetBstWindowCentered(p.MainWindowHandle);
                        this.SetBstWindowTitle(p.MainWindowHandle, title, ref stop);
                    }
                    int tries = 0;
                    while (!stop && tries < 30) {
                        BstProcess.GetAndUse(BstProcess.HDFrontendProcess, props.HDFrontendPath, (proc) => {
                            if (proc == null) {
                                stop = true;
                                fail = true;
                            } else if (proc.MainWindowHandle == IntPtr.Zero) {
                                Thread.Sleep(500);
                                tries++;
                            } else {
                                this.SetBstWindowCentered(proc.MainWindowHandle);
                                this.SetBstWindowTitle(proc.MainWindowHandle, title, ref stop);
                            }
                        });
                    }
                    if (tries >= 30) {
                        fail = true;
                    }
                } catch {
                    fail = true;
                }
                if (fail) {
                    this.Info("Process attach is successful but incomplete.");
                }
                p.Dispose();
            } else {
                this.Info("Process attach is successful but incomplete.");
            }
        }

        private void SetBstWindowTitle(IntPtr hWnd, string title, ref bool stop)
        {
            Win32Methods.SetWindowText(hWnd, title);
            stop = true;
            this.Info("Process attach fully complete.");
        }

        private void SetBstWindowCentered(IntPtr hWnd)
        {
            Rectangle rect = new Rectangle();
            if (Win32Methods.GetWindowRect(hWnd, ref rect)) {
                var screenRect = Screen.PrimaryScreen.WorkingArea;
                int w = rect.Width - rect.X;
                int h = rect.Height - rect.Y;
                Win32Methods.MoveWindow(hWnd, (screenRect.Width - w) / 2, (screenRect.Height - h) / 2, w, h, false);
                this.Info("Successfully centered BlueStacks window.");
            } else {
                this.Info("Failed to center BlueStacks window.");
            }
        }

        private void Info(object o)
        {
            if (this.logOutput) {
                Console.WriteLine(o);
                Thread.Sleep(350);
            }
        }

        private void Warn(object o)
        {
            this.Info("Warning: " + o);
        }

        private void Error(object o)
        {
            this.Info("Error: " + o);
        }

        private static void Exit(int code)
        {
            Environment.Exit(code);
        }
    }
}
