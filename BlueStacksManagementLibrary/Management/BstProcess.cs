using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using BlueStacks.Management.Config;

namespace BlueStacks.Management
{
    public static class BstProcess
    {
        public static readonly string HDServiceProcess = "HD-Service";
        public static readonly string HDFrontendProcess = "HD-Frontend";
        public static readonly string HDRestartProcess = "HD-Restart";

        public delegate void UseProcessCallback(Process p);
        public delegate bool PollTimeoutCallback();

        public static Process Get(string name, string modulePath)
        {
            if (modulePath == null) {
                throw new ArgumentNullException("Module path cannot be null.");
            }
            Process p = null;
            int cnt = 0;
            while (cnt < 30) {
                try {
                    p = Process.GetProcessesByName(name).FirstOrDefault((proc) => {
                        string moduleFileName = null;
                        try {
                            moduleFileName = proc.MainModule.FileName;
                        } catch (NullReferenceException) {
                            return false;
                        }
                        return modulePath.Equals(moduleFileName, StringComparison.InvariantCultureIgnoreCase);
                    });
                    break;
                } catch (Win32Exception e) {
                    if (e.NativeErrorCode != 299) {
                        throw;
                    }
                }
                cnt++;
                Thread.Sleep(AppConstants.ProcessPollTime);
            }
            return p;
        }

        public static void GetAndUse(string name, string modulePath, UseProcessCallback callback)
        {
            using (var p = Get(name, modulePath)) {
                callback.Invoke(p);
            }
        }

        public static Process Poll(string name, string modulePath, int maxTime, PollTimeoutCallback callback)
        {
            Process p;
            while (true) {
                int time = 0;
                do {
                    Thread.Sleep(AppConstants.ProcessPollTime);
                    p = Get(name, modulePath);
                    time += AppConstants.ProcessPollTime;
                } while (p == null && time < maxTime);
                if (p == null) {
                    if (callback != null && callback.Invoke()) {
                        continue;
                    }
                }
                break;
            }
            return p;
        }

        public static void RunAndWait(string path)
        {
            using (var p = Process.Start(path)) {
                p.WaitForExit();
            }
        }
    }
}
