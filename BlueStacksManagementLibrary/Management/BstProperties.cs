using System;
using System.IO;
using Microsoft.Win32;

namespace BlueStacks.Management
{
    public sealed class BstProperties : IDisposable
    {
        public static readonly string PatchedUpdaterExePath = new FileInfo("bstupdater.exe").FullName;

        private static readonly string BstSubkey = @"SOFTWARE\BlueStacks";
        private static readonly string SysSubkey = BstSubkey + @"\Guests\Android";
        private static readonly string VideoSubkey = SysSubkey + @"\FrameBuffer\0";

        private readonly RegistryKey bstKey;
        private readonly RegistryKey sysKey;
        private readonly RegistryKey videoKey;

        private string failedProperties;
        private bool disposed;

        public bool HasInstallDetails
        {
            get
            {
                return this.bstKey != null && this.InstallPath != null && this.DataPath != null;
            }
        }

        public bool HasSystemSettings
        {
            get
            {
                return this.sysKey != null;
            }
        }

        public bool HasVideoSettings
        {
            get
            {
                return this.videoKey != null;
            }
        }

        // Paths
        public string InstallPath
        {
            get;
            private set;
        }

        public string DataPath
        {
            get;
            private set;
        }

        public string DataLogsPath
        {
            get;
            private set;
        }

        public string HDLauncherPath
        {
            get;
            private set;
        }

        public string HDAgentPath
        {
            get;
            private set;
        }

        public string HDServicePath
        {
            get;
            private set;
        }

        public string HDFrontendPath
        {
            get;
            private set;
        }

        public string HDRestartPath
        {
            get;
            private set;
        }

        public string HDUpdaterPath
        {
            get;
            private set;
        }

        public string HDQuitPath
        {
            get;
            private set;
        }

        // Device Properties
        public int WindowWidth
        {
            get
            {
                return GetKeyValue<int>(this.videoKey, "WindowWidth");
            }

            set
            {
                this.SetKeyValue<int>(this.videoKey, "WindowWidth", value, "Window Width");
            }
        }

        public int WindowHeight
        {
            get
            {
                return GetKeyValue<int>(this.videoKey, "WindowHeight");
            }

            set
            {
                this.SetKeyValue<int>(this.videoKey, "WindowHeight", value, "Window Height");
            }
        }

        public int TabletWidth
        {
            get
            {
                return GetKeyValue<int>(this.videoKey, "GuestWidth");
            }

            set
            {
                this.SetKeyValue<int>(this.videoKey, "GuestWidth", value, "Tablet Width");
            }
        }

        public int TabletHeight
        {
            get
            {
                return GetKeyValue<int>(this.videoKey, "GuestHeight");
            }

            set
            {
                this.SetKeyValue<int>(this.videoKey, "GuestHeight", value, "Tablet Height");
            }
        }

        public int AllocatedRAM
        {
            get
            {
                return GetKeyValue<int>(this.sysKey, "Memory");
            }

            set
            {
                this.SetKeyValue<int>(this.sysKey, "Memory", value, "Allocated RAM");
            }
        }

        public bool FullScreen
        {
            get
            {
                return Convert.ToBoolean(GetKeyValue<int>(this.videoKey, "FullScreen"));
            }

            set
            {
                this.SetKeyValue<int>(this.videoKey, "FullScreen", Convert.ToInt32(value), "Full Screen");
            }
        }

        // Error Properties
        public string LastErrorMessage
        {
            get
            {
                return "There were some properties that were not saved:\n" + this.failedProperties +
                       "\nThis is most probably caused by registry access issues.";
            }
        }

        public bool HasErrorMessage
        {
            get
            {
                return this.failedProperties != null;
            }
        }

        public BstProperties()
        {
            this.bstKey = Registry.LocalMachine.OpenSubKey(BstSubkey, true);
            this.sysKey = Registry.LocalMachine.OpenSubKey(SysSubkey, true);
            this.videoKey = Registry.LocalMachine.OpenSubKey(VideoSubkey, true);
            this.InstallPath = GetKeyValue<string>(this.bstKey, "InstallDir");
            this.DataPath = GetKeyValue<string>(this.bstKey, "DataDir");
            if (this.InstallPath != null) {
                this.HDLauncherPath = CreatePath(this.InstallPath + @"\HD-StartLauncher.exe");
                this.HDServicePath = CreatePath(this.InstallPath + @"\" + BstProcess.HDServiceProcess + ".exe");
                this.HDFrontendPath = CreatePath(this.InstallPath + @"\" + BstProcess.HDFrontendProcess + ".exe");
                this.HDRestartPath = CreatePath(this.InstallPath + @"\" + BstProcess.HDRestartProcess + ".exe");
                this.HDAgentPath = CreatePath(this.InstallPath + @"\HD-Agent.exe");
                this.HDUpdaterPath = CreatePath(this.InstallPath + @"\HD-UpdaterService.exe");
                this.HDQuitPath = CreatePath(this.InstallPath + @"\HD-Quit.exe");
            }
            if (this.DataPath != null) {
                this.DataLogsPath = CreatePath(this.DataPath + @"\Logs");
            }
        }

        public string[] GetDataLogFilePaths()
        {
            return Directory.GetFiles(this.DataLogsPath, "*.log", SearchOption.TopDirectoryOnly);
        }

        public void ClearLastError()
        {
            this.failedProperties = null;
        }

        public void Dispose()
        {
            if (!this.disposed) {
                this.disposed = true;
                CloseKeyIfNotNull(this.bstKey);
                CloseKeyIfNotNull(this.sysKey);
                CloseKeyIfNotNull(this.videoKey);
                GC.SuppressFinalize(this);
            }
        }

        private void SetKeyValue<T>(RegistryKey key, string name, T value, string property)
        {
            var kind = RegistryValueKind.Unknown;
            try {
                kind = key.GetValueKind(name);
            } catch (IOException) {
            }
            if (kind != RegistryValueKind.Unknown) {
                key.SetValue(name, value, kind);
            } else {
                this.failedProperties += property + "\n";
            }
        }

        private static T GetKeyValue<T>(RegistryKey key, string name)
        {
            if (key != null) {
                object val = null;
                try {
                    val = key.GetValue(name);
                } catch (IOException) {
                }
                if (val != null) {
                    return (T)key.GetValue(name);
                }
            }
            return default(T);
        }

        private static string CreatePath(string path)
        {
            return new FileInfo(path).FullName;
        }

        private static void CloseKeyIfNotNull(RegistryKey key)
        {
            if (key != null) {
                key.Dispose();
            }
        }
    }
}
