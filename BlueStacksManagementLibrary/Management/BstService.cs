using System;
using System.Management;
using System.ServiceProcess;

namespace BlueStacks.Management
{
    public sealed class BstService : ServiceController, IDisposable
    {
        public static readonly string Android = "BstHdAndroidSvc";
        public static readonly string LogRotator = "BstHdLogRotatorSvc";
        public static readonly string Updater = "BstHdUpdaterSvc";
        public static readonly string LogRotatorName = "BlueStacks Log Rotator Service";
        public static readonly string UpdaterName = "BlueStacks Updater Service";

        private readonly ManagementObject mngObj;
        private readonly object[] mngParam = new object[2];

        private bool disposed;

        public BstService(string name) : base(name)
        {
            this.mngObj = new ManagementObject("Win32_Service.Name='" + name + "'");
        }

        public StartType StartMode
        {
            get
            {
                string type = null;
                try {
                    type = this.mngObj["StartMode"].ToString();
                } catch {
                }
                StartType mode;
                switch (type) {
                    case "Auto":
                        mode = StartType.Automatic;
                        break;
                    case "Manual":
                        mode = StartType.Manual;
                        break;
                    case "Disabled":
                        mode = StartType.Disabled;
                        break;
                    default:
                        mode = StartType.Unknown;
                        break;
                }
                return mode;
            }

            set
            {
                if (value != StartType.Unknown) {
                    this.mngParam[0] = value;
                    this.mngParam[1] = null;
                    try {
                        this.mngObj.InvokeMethod("ChangeStartMode", this.mngParam);
                    } catch {
                        throw new InvalidOperationException("Cannot change service startup type.");
                    }
                } else {
                    throw new ArgumentException("Cannot use startup type of " + value);
                }
            }
        }

        public string PathName
        {
            get
            {
                try {
                    return this.mngObj["PathName"].ToString();
                } catch {
                    return null;
                }
            }

            set
            {
                if (value != null && value.Length > 0) {
                    this.mngParam[0] = null;
                    this.mngParam[1] = value;
                    try {
                        this.mngObj.InvokeMethod("Change", this.mngParam);
                    } catch {
                        throw new InvalidOperationException("Cannot change service path name.");
                    }
                } else {
                    throw new ArgumentException("Path name must not be null and has a length > 0.");
                }
            }
        }

        public new void Dispose()
        {
            if (!this.disposed) {
                this.disposed = true;
                this.Close();
                base.Dispose();
                this.mngObj.Dispose();
            }
        }

        void IDisposable.Dispose()
        {
            this.Dispose();
        }

        public enum StartType : int
        {
            Unknown = 0,
            Automatic = 1,
            Manual = 2,
            Disabled = 3
        }
    }
}
