using System;
using System.Diagnostics;
using System.IO;
using System.Security.Principal;
using Microsoft.VisualBasic.Devices;
using ProcessPrivileges;

namespace BlueStacks.Management.Utilities
{
    public static class Computer
    {
        private static readonly ComputerInfo Info = new ComputerInfo();

        public static readonly string OSName = Info.OSFullName + " build " + Info.OSVersion;
        public static readonly int TotalRAM = Convert.ToInt32(Info.TotalPhysicalMemory / (1024 * 1024));

        public static bool OpenFolder(string path)
        {
            bool result = true;
            Process p = null;
            try {
                p = Process.Start(path);
            } catch {
                result = false;
            } finally {
                if (p != null) {
                    p.Dispose();
                }
            }
            return result;
        }

        public static string DeleteFiles(params string[] paths)
        {
            string err = null;
            if (!(paths == null || paths.Length == 0)) {
                PrivilegeEnabler priv = null;
                try {
                    priv = new ProcessPrivileges.PrivilegeEnabler(Process.GetCurrentProcess(), Privilege.TakeOwnership);
                    for (int i = 0; i < paths.Length; i++) {
                        string s = paths[i];
                        var inf = new FileInfo(s);
                        try {
                            var sec = inf.GetAccessControl();
                            sec.SetOwner(WindowsIdentity.GetCurrent().User);
                            inf.SetAccessControl(sec);
                            inf.Delete();
                        } catch (Exception e) {
                            err += s + ": " + e.Message + ((i < paths.Length - 1) ? "\n" : "");
                        }
                    }
                } catch (Exception e) {
                    err += "DeleteFiles: " + e.Message;
                } finally {
                    if (priv != null) {
                        priv.Dispose();
                    }
                }
            }
            return err;
        }
    }
}
