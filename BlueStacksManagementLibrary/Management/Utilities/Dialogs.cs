﻿using System.Windows.Forms;

namespace BlueStacks.Management.Utilities
{
    public static class Dialogs
    {
        public static void InfoOK(string msg, string title)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static bool InfoOKCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK;
        }

        public static bool InfoYesNo(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes;
        }

        public static bool InfoRetryCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Information) == DialogResult.Retry;
        }

        public static void QuestionOK(string msg, string title)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Question);
        }

        public static bool QuestionOKCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK;
        }

        public static bool QuestionYesNo(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        }

        public static bool QuestionRetryCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry;
        }

        public static void WarnOK(string msg, string title)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static bool WarnOKCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK;
        }

        public static bool WarnYesNo(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes;
        }

        public static bool WarnRetryCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning) == DialogResult.Retry;
        }

        public static void ErrorOK(string msg, string title)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static bool ErrorOKCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK;
        }

        public static bool ErrorYesNo(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes;
        }

        public static bool ErrorRetryCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry;
        }

        public static void PlainOK(string msg, string title)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        public static bool PlainOKCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.None) == DialogResult.OK;
        }

        public static bool PlainYesNo(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.None) == DialogResult.Yes;
        }

        public static bool PlainRetryCancel(string msg, string title)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.None) == DialogResult.Retry;
        }

        public static void InfoOK(IWin32Window owner, string msg, string title)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static bool InfoOKCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK;
        }

        public static bool InfoYesNo(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes;
        }

        public static bool InfoRetryCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Information) == DialogResult.Retry;
        }

        public static void QuestionOK(IWin32Window owner, string msg, string title)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.Question);
        }

        public static bool QuestionOKCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK;
        }

        public static bool QuestionYesNo(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        }

        public static bool QuestionRetryCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry;
        }

        public static void WarnOK(IWin32Window owner, string msg, string title)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static bool WarnOKCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK;
        }

        public static bool WarnYesNo(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes;
        }

        public static bool WarnRetryCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning) == DialogResult.Retry;
        }

        public static void ErrorOK(IWin32Window owner, string msg, string title)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static bool ErrorOKCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK;
        }

        public static bool ErrorYesNo(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes;
        }

        public static bool ErrorRetryCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry;
        }

        public static void PlainOK(IWin32Window owner, string msg, string title)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        public static bool PlainOKCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.None) == DialogResult.OK;
        }

        public static bool PlainYesNo(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.None) == DialogResult.Yes;
        }

        public static bool PlainRetryCancel(IWin32Window owner, string msg, string title)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.None) == DialogResult.Retry;
        }

        public static void InfoOK(string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information, (MessageBoxDefaultButton)defChoice);
        }

        public static bool InfoOKCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK;
        }

        public static bool InfoYesNo(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes;
        }

        public static bool InfoRetryCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Information) == DialogResult.Retry;
        }

        public static void QuestionOK(string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Question, (MessageBoxDefaultButton)defChoice);
        }

        public static bool QuestionOKCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK;
        }

        public static bool QuestionYesNo(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        }

        public static bool QuestionRetryCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry;
        }

        public static void WarnOK(string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Warning, (MessageBoxDefaultButton)defChoice);
        }

        public static bool WarnOKCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK;
        }

        public static bool WarnYesNo(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes;
        }

        public static bool WarnRetryCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning) == DialogResult.Retry;
        }

        public static void ErrorOK(string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error, (MessageBoxDefaultButton)defChoice);
        }

        public static bool ErrorOKCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK;
        }

        public static bool ErrorYesNo(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes;
        }

        public static bool ErrorRetryCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry;
        }

        public static void PlainOK(string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.None, (MessageBoxDefaultButton)defChoice);
        }

        public static bool PlainOKCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.None) == DialogResult.OK;
        }

        public static bool PlainYesNo(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.None) == DialogResult.Yes;
        }

        public static bool PlainRetryCancel(string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.None) == DialogResult.Retry;
        }

        public static void InfoOK(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.Information, (MessageBoxDefaultButton)defChoice);
        }

        public static bool InfoOKCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Information, (MessageBoxDefaultButton)defChoice) == DialogResult.OK;
        }

        public static bool InfoYesNo(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Information, (MessageBoxDefaultButton)defChoice) == DialogResult.Yes;
        }

        public static bool InfoRetryCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Information, (MessageBoxDefaultButton)defChoice) == DialogResult.Retry;
        }

        public static void QuestionOK(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.Question, (MessageBoxDefaultButton)defChoice);
        }

        public static bool QuestionOKCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, (MessageBoxDefaultButton)defChoice) == DialogResult.OK;
        }

        public static bool QuestionYesNo(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question, (MessageBoxDefaultButton)defChoice) == DialogResult.Yes;
        }

        public static bool QuestionRetryCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question, (MessageBoxDefaultButton)defChoice) == DialogResult.Retry;
        }

        public static void WarnOK(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.Warning, (MessageBoxDefaultButton)defChoice);
        }

        public static bool WarnOKCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, (MessageBoxDefaultButton)defChoice) == DialogResult.OK;
        }

        public static bool WarnYesNo(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, (MessageBoxDefaultButton)defChoice) == DialogResult.Yes;
        }

        public static bool WarnRetryCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning, (MessageBoxDefaultButton)defChoice) == DialogResult.Retry;
        }

        public static void ErrorOK(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error, (MessageBoxDefaultButton)defChoice);
        }

        public static bool ErrorOKCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.Error, (MessageBoxDefaultButton)defChoice) == DialogResult.OK;
        }

        public static bool ErrorYesNo(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Error, (MessageBoxDefaultButton)defChoice) == DialogResult.Yes;
        }

        public static bool ErrorRetryCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, (MessageBoxDefaultButton)defChoice) == DialogResult.Retry;
        }

        public static void PlainOK(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OK, MessageBoxIcon.None, (MessageBoxDefaultButton)defChoice);
        }

        public static bool PlainOKCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.OKCancel, MessageBoxIcon.None, (MessageBoxDefaultButton)defChoice) == DialogResult.OK;
        }

        public static bool PlainYesNo(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.None, (MessageBoxDefaultButton)defChoice) == DialogResult.Yes;
        }

        public static bool PlainRetryCancel(IWin32Window owner, string msg, string title, Choice defChoice)
        {
            return DialogsImpl.Show(owner, msg, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.None, (MessageBoxDefaultButton)defChoice) == DialogResult.Retry;
        }

        public enum Choice : int
        {
            Choice1 = MessageBoxDefaultButton.Button1,
            Choice2 = MessageBoxDefaultButton.Button2
        }
    }
}
